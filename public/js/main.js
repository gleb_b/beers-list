//* LOADING BEERS LIST FROM JSON  *// 
function jsonList(params, reset) {

    $.getJSON('https://api.punkapi.com/v2/beers', params, function(data) {

        if (reset) {
            $('.beer').remove();
            pages = 1;
        }

        /* Outputs JSON response as cards templates  */
        formCards(data);

    });


    /* FORMING LOADMORE REQUEST AND BUTTON */

    nextElements = 0;

    /* Checking previously requested name search */
    value = $('#nameSearch').val();
    if (value.length >= 4) {
        params = {
            "per_page": 24,
            "beer_name": value,
            "page": pages + 1
        };
    } else {
        params = {
            "per_page": 24,
            "page": pages + 1
        };
    }

    $.getJSON('https://api.punkapi.com/v2/beers', params, function(data){
    	/* Forming LoadMore Button */
    	formLoadMore(data);
    } );
}




	/* Outputs JSON response as cards templates  */
    function formCards(data){
    	        $.each(data, function(key, val) {

            /* BEER CARDS -- Handlebars Templating*/

            var beer = {
                beer_title: val["name"],
                beer_img: val["image_url"],
                beer_tagline: val["tagline"],
                beer_description: val["description"],
                beer_tips: val["brewers_tips"],
                beer_ibu: val["ibu"],
                beer_abv: val["abv"],
                beer_id: val["id"]
            };

            var templateScript = $('#beerCard').html();
            var template = Handlebars.compile(templateScript);
            $('.beers').append(template(beer));
            orderSort(); /* keeps chosen sorting after appending new items */

        });

    }

/* Forming LoadMore Button */
function formLoadMore(data) {

        $.each(data, function(key, val) {
            nextElements++;
        });

        $('#loadMore').unbind('click');
        $('#loadMore').remove();

        var moreClass; // Load More button class 
        if (nextElements == 0) {
            moreClass = "invisible";
        }

        loadMoreTMPLT = {
            more_items: nextElements,
            more_class: moreClass
        };
        var templateScript = $('#loadMoreTMPLT').html();
        var template = Handlebars.compile(templateScript);
        $('.load-more-wrapper').append(template(loadMoreTMPLT));


        $('#loadMore').click(function() {
            value = $('#nameSearch').val();
            if (value.length >= 4) {
                pages++;
                jsonList({
                    "per_page": 24,
                    "beer_name": value,
                    "page": pages
                }, false);
            } else {
                $('.beers').addClass('empty-search');
                pages++;
                jsonList({
                    "per_page": 24,
                    "page": pages
                }, false);
            }
        });
    }




/* NAME FILTERING */

$('#nameSearch').on('input', function() {
    var value = $(this).val();
    if (value.length >= 4) {
        ajaxNameSearch(value);
    }
    if (value.length == 0) {
        jsonList({
            "per_page": "24"
        });
    }
});

function ajaxNameSearch(value) {
    jsonList({
        "per_page": 24,
        "beer_name": value
    }, true);
}


//* ORDER SORTING *//


$('#orderSelect').on('change', function() {
    orderSort();
});

function orderSort() {
    if ($('#orderSelect').val() == 'AB') {
       sortByDataAttribute('title', 'ASC');
    }
    if ($('#orderSelect').val() == 'BA') {
       sortByDataAttribute('title', 'DESC');
    }
    if ($('#orderSelect').val() == 'ASCabv') {
        sortByDataAttribute('abv', 'ASC', 'float');
    }
    if ($('#orderSelect').val() == 'DESCabv') {
        sortByDataAttribute('abv', 'DESC', 'float');
    }
}

/* Sorts cards by specified data-attribute and ASC/DESC order; 'dataType' used only for float attributes  */
function sortByDataAttribute(dataAttribute, order, dataType) { 
    var beers = $('.beers'),
        beer = $('.beer'),
        dataAttribute = 'data-'+ dataAttribute;
        orderValue = 1;
            if (order == 'DESC'){
    	orderValue = -1;
    }

    beer.sort(function(a, b) {

    	if (dataType == 'float'){
        	var an = parseFloat(a.getAttribute('data-abv'), 10),
        	   	bn = parseFloat(b.getAttribute('data-abv'), 10);
        }

        else {
        	var an = a.getAttribute('data-title'),
         	 	bn = b.getAttribute('data-title');
		}

        if (an > bn) {return 1*orderValue;}
        if (an < bn) {return -1*orderValue;}

        return 0;
    });
    beer.detach().appendTo(beers);
}




//** INITIAL LOADING **//
var pages = 1,
	nextElements = 0;
jsonList({
    "per_page": "24"
});